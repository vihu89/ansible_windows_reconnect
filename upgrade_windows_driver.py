﻿import glob
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager


class WindowsDriverInstaller(object):
    def __init__(self,
                 module_path='/path/to/modules',
                 inventory_file='path/to/inventoruy',
                 remote_user='UserNameRemoteHost',
                 remote_password='Password'):
        self._variable_manager = VariableManager()
        self._loader = DataLoader()
        Options = namedtuple('Options',
                             ['listtags',
                              'listtasks',
                              'listhosts',
                              'syntax',
                              'connection',
                              'module_path',
                              'forks',
                              'remote_user',
                              'private_key_file',
                              'ssh_common_args',
                              'ssh_extra_args',
                              'sftp_extra_args',
                              'scp_extra_args',
                              'become',
                              'become_method',
                              'become_user',
                              'verbosity',
                              'check'])
        self._options = Options(listtags=False,
                                listtasks=False,
                                listhosts=False,
                                syntax=False,
                                connection='winrm',
                                module_path=None,
                                forks=100,
                                remote_user=remote_user,
                                private_key_file=None,
                                ssh_common_args=None,
                                ssh_extra_args=None,
                                sftp_extra_args=None,
                                scp_extra_args=None,
                                become=True,
                                become_method=None,
                                become_user=remote_user,
                                verbosity=None,
                                check=False
                                )
        self._inventory = Inventory(loader=self._loader,
                                    variable_manager=self._variable_manager,
                                    host_list=inventory_file)
        self._variable_manager.set_inventory(self._inventory)

        self._variable_manager.extra_vars = {'ansible_user': remote_user,
                                             'ansible_port': '5986',
                                             'ansible_connection': 'winrm',
                                             'ansible_password': remote_password,
                                             'ansible_winrm_server_cert_validation': 'ignore',
                                             'ansible_winrm_transport': 'ssl'}

        self._passwords = {'ansible_password': remote_password}

    def run(self, hosts='all', driver_path='path/to/driver', import_certificate=False):
        ''' Method to run the driver installation on remote hosts
            @hosts (string):            specify which hosts to run the test on. taken from inventory file
            @driver_path (string):      full path to the driver directory, could be local or mounted NFS Share
            @import_certificate (bool): whether to import the certificate for driver installation, must be
                                        present in the driver_path directory
        '''
        try:
            # ideally the last item in the driver_path should be the directory containing the driver version
            driver_version = driver_path.split('/')[-1]
            driver_file = glob.glob(driver_path + '/*.inf')[0].split('/')[-1]
            driver_path_remote = 'C:\\{0}\\{1}'.format(driver_version, driver_file)
        except Exception, e:
            raise Exception('Unable to get the driver version from driver_path directory', e)

        if import_certificate:
            # if you want to import the certificate on the remote host then you must put it in the driver_path
            assert (glob.glob(driver_path + '/*.cer') != []), 'Copy the certificate to driver_path directory'
            play_source = dict(name='Copy and install windows driver on hosts',
                               hosts=hosts,
                               tasks=[dict(action=dict(module='win_ping'),
                                           name='ping the servers before the test starts'),
                                      dict(action=dict(module='raw',
                                                       args='Get-NetAdapter | Select-Object InterfaceDescription,\
                                                             DriverVersionString'),
                                           name='get netadapter stats before test starts',
                                           register='netadapter_before'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{netadapter_before.stdout_lines}}'))),
                                      dict(action=dict(module='win_copy',
                                                       args=dict(src=driver_path,
                                                                 dest='c:\\',
                                                                 msg='{{netadapter_before.stdout_lines}}')),
                                           name='copy the driver directory to host',
                                           register='copy_driver'),
                                      dict(action=dict(module='debug', args=dict(msg='{{copy_driver}}'))),
                                      dict(action=dict(module='raw',
                                                       args='(gci -r c:\\{d}\ -filter *.inf).fullname'.
                                                            format(d=driver_version)),
                                           name='get driver file from c:',
                                           register='driver_filename'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{driver_filename.stdout_lines[0]}}'))),
                                      dict(action=dict(module='raw',
                                                       args='(gci -r c:\\{d}\ -filter *.cer).fullname'.
                                                            format(d=driver_version)),
                                           name='get certificate from c drive',
                                           register='certificate_filename'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{certificate_filename.stdout_lines[0]}}'))),
                                      dict(action=dict(module='raw',
                                                       args='Import-Certificate -FilePath {{certificate_filename.stdout_lines[0]}}\
                                                            -CertStoreLocation Cert:\LocalMachine\TrustedPublisher'),
                                           name='import certificate to trusted publishers on the remote host',
                                           register='certificate'),
                                      dict(action=dict(module='debug', args=dict(msg='{{certificate}}'))),
                                      dict(action=dict(module='win_reconnect',
                                                       args=dict(driver_file_path=driver_path_remote)),
                                           register='install'),
                                      dict(action=dict(module='win_ping'),
                                           name='ping the servers after the test'),
                                      dict(action=dict(module='raw',
                                                       args='Get-NetAdapter | Select-Object InterfaceDescription,\
                                                             DriverVersionString'),
                                           name='get netadapter stats after test',
                                           register='netadapter_after'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{netadapter_after.stdout_lines}}')))
                                      ]
                               )
        else:
            play_source = dict(name='Copy and install windows driver on hosts',
                               hosts=hosts,
                               tasks=[dict(action=dict(module='win_ping'),
                                           name='ping the servers before the test starts'),
                                      dict(action=dict(module='raw',
                                                       args='Get-NetAdapter | Select-Object InterfaceDescription,\
                                                             DriverVersionString'),
                                           name='get netadapter stats before test starts',
                                           register='netadapter_before'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{netadapter_before.stdout_lines}}'))),
                                      dict(action=dict(module='win_copy',
                                                       args=dict(src=driver_path,
                                                                 dest='c:\\',
                                                                 msg='{{netadapter_before.stdout_lines}}')),
                                           name='copy the driver directory to host',
                                           register='copy_driver'),
                                      dict(action=dict(module='debug', args=dict(msg='{{copy_driver}}'))),
                                      dict(action=dict(module='raw',
                                                       args='(gci -r c:\\{d}\ -filter *.inf).fullname'.
                                                            format(d=driver_version)),
                                           name='get driver file from c:',
                                           register='driver_filename'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{driver_filename.stdout_lines[0]}}'))),
                                      dict(action=dict(module='win_reconnect',
                                                       args=dict(driver_file_path=driver_path_remote)),
                                           register='install'),
                                      dict(action=dict(module='win_ping'),
                                           name='ping the servers after the test'),
                                      dict(action=dict(module='raw',
                                                       args='Get-NetAdapter | Select-Object InterfaceDescription,\
                                                             DriverVersionString'),
                                           name='get netadapter stats after test',
                                           register='netadapter_after'),
                                      dict(action=dict(module='debug',
                                                       args=dict(msg='{{netadapter_after.stdout_lines}}')))
                                      ]
                               )

        play = Play().load(play_source, variable_manager=self._variable_manager, loader=self._loader)

        tqm = None
        try:
            tqm = TaskQueueManager(inventory=self._inventory,
                                   variable_manager=self._variable_manager,
                                   loader=self._loader,
                                   options=self._options,
                                   passwords=self._passwords,
                                   stdout_callback='default',
                                   )

            result = tqm.run(play)
            return result
        finally:
            if tqm is not None:
                tqm.cleanup()

if __name__ == '__main__':
    installer = WindowsDriverInstaller()
    installer.run(hosts='server', import_certificate=False)
